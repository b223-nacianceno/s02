<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>S02: Repetition Control Structures and Array Manipulation</title>
	</head>
	<body>
		<h1>Repetition Control Structures</h1>

		<h2>While Loop</h2>
		<?php whileLoop(); ?>

		<h2>Do-While Loop</h2>
		<?php doWhileLoop(); ?>

		<h2>For Loop</h2>
		<?php forLoop(); ?>

		<h2>Continue and Break</h2>
		<?php modifiedForLoop(); ?>

		<h1>Array Manipulation</h1>

		<h2>Types of Array</h2>

		<h3>Simple Array</h3>
		<!-- 
			Syntax:
			foreach($array as $value/element){
				//code to be executed.
			}
		 -->
		 <ul>
		 	<?php foreach($computerBrands as $brand) { ?>
		 		<li><?= $brand; ?></li>
		 	<?php } ?>
		 </ul>

		<h3>Associative Array</h3>
		<!-- 
			Syntax:
			foreach($array as $key=>$value){
				//code to be executed.
			}
		 -->
		<ul>
			<?php foreach($gradePeriods as $period => $grade){ ?>
				<li>
					Grade in <?= $period; ?> is <?= $grade; ?>
				</li>
			<?php } ?>
		</ul>

		<h3>Multidimensional Array</h3>
		<!-- 

			var_dump() vs print_r()

			var_dump()
				- displays structured information about variables/expressions including its type and value.

			print_r()
				- displays information about a variable in a way that's readable by humans.

		 -->
		<!-- <pre><?php print_r($heroes); ?></pre> -->
		<ul>
			<?php
				forEach($heroes as $team){
					forEach($team as $member){
			?>
				<li><?= $member?></li>
			<?php
					}
				}
			?>
		</ul>

		<h3>Multidimensional Associative Array</h3>
		<ul>
			<?php
				forEach($ironManPowers as $label => $powerGroup){
					forEach($powerGroup as $power){
			?>
				<li><?= "$label: $power"; ?></li>
			<?php
					}
				}
			?>
		</ul>

		<h2>Array Functions</h2>

		<h3>Original Array</h3>
		<pre><?php print_r($computerBrands)?></pre>

		<h3>Sorting</h3>
		<pre><?php print_r($sortedBrands); ?></pre>

		<h3>Sorting (Reverse)</h3>
		<pre><?php print_r($reverseSortedBrands); ?></pre>

		<h3>Append</h3>
		<?php array_push($computerBrands, 'Apple'); ?>
		<pre><?php print_r($computerBrands); ?></pre>

		<?php array_unshift($computerBrands, 'Dell'); ?>
		<pre><?php print_r($computerBrands); ?></pre>

		<h3>Remove</h3>
		<?php array_pop($computerBrands); ?>
		<pre><?php print_r($computerBrands); ?></pre>

		<?php array_shift($computerBrands); ?>
		<pre><?php print_r($computerBrands); ?></pre>

		<h3>Others</h3>

		<h4>Count</h4>

		<pre><?php echo count($computerBrands); ?></pre>

		<h4>In array</h4>
		<p><?php echo searchBrand($computerBrands, 'HP'); ?></p>
		<p><?php echo searchBrand($computerBrands, 'Acer'); ?></p>

		<h4>Reverse (not on descending order)</h4>
		<pre><?php print_r($gradePeriods); ?></pre>
		<pre><?php print_r($reversedGradePeriods); ?></pre>

		<pre><?php print_r($gradePeriodsCopy); ?></pre>

	</body>
</html>
